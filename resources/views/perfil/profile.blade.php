@extends('layouts.master')
	<link rel="stylesheet" href="{{ url('/assets/css/profile_style.css') }}" >
@section('titulo')
	PERFIL {{$datosUsuario->name}}
@endsection
@section('contenido')

	<body>
		<div class="container emp-profile">
            <form method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-img mr-5">
                            <img src="https://www.gravatar.com/avatar/{{$datosUsuario->photo}}?&s=290" class="rounded-circle">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="profile-head">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h5>{{$datosUsuario->name}} {{$datosUsuario->surname}}</h5>                                 
                                </div>
                                <div class="col-sm-4">
                                    <a type="button" class="profile-edit-btn" href="{{ url('profile/edit') }}/{{$datosUsuario->id}}">Editar</a>
                                </div>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sobre mi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Otros datos</a>
                                </li>
                                @if ($datosUsuario->numero_partituras->count() > 0)
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#part" role="tab" aria-controls="profile" aria-selected="false">Datos partituras</a>
                                    </li>
                                @endif
                                @if ($datosUsuario->numero_analisis->count() > 0)
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#an" role="tab" aria-controls="profile" aria-selected="false">Datos análisis</a>
                                    </li>
                                @endif
                        </div>

                        <div class="tab-content profile-tab">
                            <div class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>ID de Usuario</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->id}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Nombre</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->name}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Apellidos</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->surname}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->email}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Contraseña</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>
                                            <input type="password" name="password" value="{{ Str::limit($datosUsuario->password, 4, $end='...') }}" id="password" class="form-control-plaintext" readonly>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Fecha de registro</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->created_at}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Partituras añadidas</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>
                                            {{$datosUsuario->numero_partituras->count()}}
                                            @if ($datosUsuario->numero_partituras->count() == 1)
                                                partitura
                                            @else
                                                partituras
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Análisis añadidos</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>
                                            {{$datosUsuario->numero_analisis->count()}} análisis
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Descargas disponibles</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{$datosUsuario->descargasDisponibles}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="part" aria-labelledby="profile-tab">
                                <table class="table table-striped tabla-float">
                                    <thead>
                                        <th>Nombre</th>
                                        <th>Fecha subida</th>
                                        <th>Nº comentarios</th>
                                    </thead>
                                    <tbody>
                                    @foreach($datosPartituras as $p)
                                    <tr>
                                        <td>
                                            <a class="link-info" href="{{ url('/partituras/ver/' . $p['slugpartitura'] . '/' . $p['id'] ) }}">{{$p->nombre}}</a>
                                        </td>
                                        <td>{{ $p->created_at }}</td>
                                        <td class="text-center">
                                            {{ $p->comentariosPartitura->count() }} 
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="an" aria-labelledby="profile-tab">
                                <table class="table table-striped tabla-float">
                                    <thead>
                                        <th>Nombre</th>
                                        <th>Fecha subida</th>
                                        <th>Comentarios</th>
                                        <th class="text-center">
                                            <span class="iconify" data-icon="clarity:heart-solid" data-inline="false" style="color: red;" data-width="20px" data-height="20px">
                                        </th>
                                        <th class="text-center">
                                            <span class="iconify" data-icon="clarity:heart-broken-solid" data-inline="false" style="color: black;" data-width="20px" data-height="20px">
                                        </th>
                                    </thead>
                                    <tbody>
                                    @foreach($datosAnalisis as $a)
                                    <tr>
                                        <td>
                                            <a class="link-info" href="{{ url('/analisis/ver/' . $a['sluganalisis'] . '/' . $a['id'] ) }}">{{$a->nombre}}</a>
                                        </td>
                                        <td>{{ $a->created_at }}</td>
                                        <td class="text-center">
                                            {{ $a->comentariosAnalisis->count() }} 
                                        </td>
                                        <td class="text-center">
                                            {{ $a->likesAnalisis->count() }} 
                                        </td> 
                                        <td class="text-center">
                                            {{ $a->dislikesAnalisis->count() }} 
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>           
        </div>
	</body>

@endsection
