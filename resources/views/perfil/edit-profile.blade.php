@extends('layouts.master')
	<link rel="stylesheet" href="{{ url('/assets/css/profile_style.css') }}" >
    
@section('titulo')
	EDITAR PERFIL 
@endsection
@section('contenido')

	<body>
		<div class="container emp-profile">
			<form action="{{ url('profile/edit') }}/{{$datosUsuario->id}}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-img">
                            <div class="profile-img mr-5">
                                <img src="https://www.gravatar.com/avatar/{{$datosUsuario->photo}}?&s=290" class="rounded-circle">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="profile-head">
                                    <h5>
                                        {{$datosUsuario->name}} {{$datosUsuario->surname}}
                                    </h5>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sobre mi</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>ID de Usuario</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>{{$datosUsuario->id}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Nombre</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="name" value="{{ $datosUsuario->name }}" id="name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Apellidos</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="surname" value="{{ $datosUsuario->surname }}" id="surname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Email</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="email" value="{{ $datosUsuario->email }}" id="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Contraseña</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="password" name="password" value="{{ $datosUsuario->password }}" id="password" class="form-control">
                                            </div>
                                        </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info" style="padding:8px 40px; margin-top:25px;" value="Modificar usuario">
                                <a href="{{url('profile')}}" class="btn btn-secondary" style="padding:8px 50px; margin-top:25px;">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>           
        </div>

	</body>

@endsection
