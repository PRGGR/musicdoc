<nav class="navbar navbar-expand-md navbar-light fixed-top mb-5">

<a class="navbar-brand" href="{{ url('home') }}">
  <img src="{{ asset('assets/logo/logo.svg') }}" width="200" height="50" alt="home">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarCollapse">
  @if(Auth::check() )
  <ul class="navbar-nav mr-auto">

    <li class="nav-item">
      <a href="{{url('/partituras')}}" class="nav-link {{ Request::is('partituras*')? ' active' : ''}}">Listado de partituras</a>
    </li>

    <li class="nav-item">
      <a href="{{url('/analisis')}}" class="nav-link {{ Request::is('analisis*')? ' active' : ''}}">Listado de análisis</a>
    </li>
  </ul>
      {{-- <i class="fa fa-search" class="mr-5" aria-hidden="true"></i>
      <form class="form-inline ml-3 mt-md-0">
        <input id="busqueda" class="form-control mt-3 mr-sm-3" type="search" name="busqueda" placeholder="Buscar" aria-label="Buscar">
          <div id="resultadoBusqueda" class="dropdown-menu">
            <a class="dropdown-item" href="#">Hola esto es una prueba</a>
          </div>
          <script>
              $(document).ready(function(){
                $('#busqueda').autocomplete({
                  source : function( request, result){
                    $.ajax({
                      type : 'POST',
                      url : '{{url('busquedaAjax')}}',
                      dataType :'json',
                      data : { '_token': '{{ csrf_token() }}','busqueda' : request['term'] },
                      success : function(data){ 
                        /*for (var i = 0; i < data.length; i++) {
                          $('#resultadoBusqueda').append('<a class="dropdown-item" href="#">'+ data[i].label + '</a>');
                        }
                        $('#busqueda').dropdown();*/
                        result(data);
                      },      
                      
                      error : function(xhr, status){
                        alert("No se han encontrado coincidencias");            
                      }
                    });
                  },
                });
              });
            </script>
          </form> --}}
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
              <img src="https://www.gravatar.com/avatar/{{$datosUsuario->photo}}?&s=35" class="rounded-circle mr-3"> 
              {{$datosUsuario->name}}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="{{ url('/profile') }}">Perfil</a>
              <a href="{{url('/partituras/crear')}}" class="dropdown-item" rel="modal:open">Nueva partitura</a>
              <a href="{{url('/analisis/crear')}}" class="dropdown-item" rel="modal:open">Nuevo análisis</a>

              <div class="dropdown-divider"></div>

              <a class="dropdown-item nav-item" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" >
              <span class="glyphicon glyphicon-off"></span> Cerrar sesión</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </div>

@else
<ul class="navbar-nav navbar-right">
<li class="nav-item">
  <a href="{{url('login')}}" class="nav-link">Login</a>
</li>
</ul>
<ul class="navbar-nav navbar-right">
<li class="nav-item">
  <a class="nav-link">/</a>
</li>
</ul>
<ul class="navbar-nav navbar-right">
<li class="nav-item">
  <a href="{{url('register')}}" class="nav-link">Registro</a>
</li>
</ul>

@endif 
</div>
</nav>






