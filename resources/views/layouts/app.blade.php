@extends('layouts.master')

@section('contenido')
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Musicdoc') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/logo/favicon.png') }}">
    <link rel="shortcut icon" sizes="192x192" href="{{ asset('assets/logo/favicon.png') }}">
    <title>@yield('titulo')</title>
</head>
<body>
    <div id="app">
        <div class="container mt-3">
            <div class="form-group mt-5"> 
            <main class="py-4 mt-5">
                @yield('content')
            </main>
            </div>
        </div>
    </div>
</body>
</html>
