<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="_token" content="{{ csrf_token() }}" />

  <!-- CSS PAULA -->
  <link rel="stylesheet" href="{{ url('/assets/css/home/master_style.css') }}" >
  <link rel="stylesheet" href="{{ url('/assets/css/home/navbar_style.css') }}" >
  <link rel="stylesheet" href="{{ url('/assets/css/footer/footer_style.css') }}" >
  <link rel="stylesheet" href="{{ url('/assets/css/tooltip_style.css') }}" >
  <link rel="stylesheet" href="{{ url('/assets/css/upbtn_style.css') }}" >

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.css') }}" > 
  
  <!-- JQuery - AJAX -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

  <!-- jQuery Modal -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 
  <!-- FAVICON -->
  <link rel="shortcut icon" type="image/png" href="{{ asset('assets/logo/favicon.png') }}">
  <link rel="shortcut icon" sizes="192x192" href="{{ asset('assets/logo/favicon.png') }}">

  <title>@yield('titulo')</title>

</head>
<body>
  <div class="container">
  <header class="row mb-5">
    @include("partials.navbar")
    </header>
      <div class="errores">
        @if(count($errors)>0)
        <ul class="pl-5 alert alert-danger" role="alert">
          @foreach($errors->all() as $error)
          <li> {{$error}} </li>
          @endforeach
        </ul>
        @endif
        @if(session()->has('success'))
          <ul class="pl-5 alert alert-success" role="alert">
              <li> {{ session()->get('success') }} </li>
          </ul>
        @endif
        @if(isset($msg))
          <ul class="pl-5 alert alert-info" role="alert">
            <li> {{ $msg }} </li>
          </ul>
        @endif
      </div>
    <div id="main" class="row disableRightClick">
      @yield('contenido')
    </div>
  </div>
  @include('partials.footer')


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Bootstrap JS -->
  <script src="{{url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>

  <!-- JavaScript PAULA-->
  <script src="{{url('/js/uppbtn.js') }}"></script>
  <script src="{{url('/js/disableright.js') }}"></script>

  <!-- Iconos -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>



</body>
</html>
