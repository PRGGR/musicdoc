<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.css') }}" >
    <link rel="stylesheet" href="{{ url('/assets/css/welcome/welcome_style.css') }}" >

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/logo/favicon.png') }}">
    <link rel="shortcut icon" sizes="192x192" href="{{ asset('assets/logo/favicon.png') }}">

    <title>@yield('titulo')</title>
  </head>
  <body>
    
    <div class="container-fluid">
      @yield("contenido")

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="{{url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
  </body>
</html>