<!DOCTYPE html>
<html>
<head>
    <title>Partitura - {{$partitura->nombre}}</title>
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.css') }}" > 
    <link rel="stylesheet" href="{{ url('/assets/css/pdf/pdf_style.css') }}" >
</head>

<body>
    <header>
        <img src="{{ asset('assets/logo/logo.svg') }}" height="50">
    </header>
    <div class="container">
        <h1 class="mb-4">{{$partitura->nombre}}</h1>

        <label for="autor">Autor: </label>
        <p>{{$partitura->autor}}</p>
        <label for="autor">Época: </label>
        <p>{{$partitura->epoca}}</p>
        <label for="descripcion">Descripción</label>
        <p>- {{$partitura->descripcion}}</p>

        @foreach($fotospartitura as $foto)
        <img src="{{ asset('assets/imagenes/partituras/' . $foto->nombrearchivo)}}" alt="{{ $foto->nombrearchivo }}" width="600" alt="">
        @endforeach
        
    </div>
    

</body>
</html>