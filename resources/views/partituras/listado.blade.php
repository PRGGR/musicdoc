@extends('layouts.master')
<link rel="stylesheet" href="{{ url('/assets/css/listado/listado_style.css') }}" >
@section('titulo')
Listado partituras
@endsection
@section('contenido')

<div class="container mt-4 mb-5">
  <h1 class="my-4">Listado
    <small>de Partituras</small>
  </h1>
  <div id="sidebar" class="mt-4">
    <h3>FILTROS</h3>
    <h5>Ordenar por</h5>
    <form action="{{ action('PartituraController@getFiltros') }}" enctype="multipart/form-data" method="post">
      {{ csrf_field() }} 
      <div class="radiob">
        <input type="radio" id="nombre" name="orden" value="nombre">
        <label for="nombre">Nombre</label><br>
        <input type="radio" id="fecha" name="orden" value="fecha">
        <label for="fecha">Fecha de subida</label><br>
        <input type="radio" id="comments" name="orden" value="comments">
        <label for="comments">Comentarios</label>
      </div>
      <input type="submit" class="btn btn-info" value="Aplicar filtros">
    </form>
  </div>

  <form action="{{ action('PartituraController@partiturasBuscadas') }}" enctype="multipart/form-data" method="post">
    <div class="row">
      <div class="input-group rounded ml-4 mr-4">
          {{ csrf_field() }} 
          <div class="col-1">  
            <button type="submit" name="buscar" class="btn btn-outline-info btn-lg" >
              <i class="fa fa-search"></i>
            </button>
          </div>
          <div class="col">
            <input type="text" id="busqueda" class="form-control rounded" name="busqueda" placeholder="Buscar" aria-label="Buscar">
          </div>
      </div>
    </div>
  </form>
      <script>
        $(document).ready(function(){
          $('#busqueda').autocomplete({
            source : function( request, result){
              $.ajax({
                type : 'POST',
                url : '{{ url('busquedaAjax') }}',
                dataType :'json',
                data : { '_token': '{{ csrf_token() }}','busqueda' : request['term'] },
                success : function(data){ 
                  result(data);
                },      
                error : function(xhr, status){
                  alert("No se han encontrado coincidencias");            
                }
              });
            },
          });
        });
      </script>
  <div class="row mb-5 mt-4 disableRightClick">
    @foreach($listadoPartiturasFoto as $partitura)
    <div class="col-lg-4 col-md-3 mb-4">
      <div class="imagebox" style="width: 14rem; height: 19rem;">
        <embed class="card-img-top category-banner img-fluid" src="assets/imagenes/partituras/{{$partitura['nombrearchivo']}}"/>
        <a href="{{ url('/partituras/ver/' . $partitura['slugpartitura'] . '/' . $partitura['id'] ) }}#page=1" ><span class="imagebox-desc">{{$partitura['nombre']}}<br> {{$partitura['autor']}}</span></a>
      </div>
    </div>
    @endforeach
  </div>

  @endsection
