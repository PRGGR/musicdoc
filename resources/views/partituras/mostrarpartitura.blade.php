@extends('layouts.master')
	<link rel="stylesheet" href="{{ url('/assets/css/mostrar/mostrar_style.css') }}" >
@section('titulo')
	Mostrar
@endsection
@section('contenido')

<body class="mb-5">
    <div class="container mt-4 mb-5">
		<div class="row mt-4">		
			<div class="col-md-4">
				<h3>{{$partitura->nombre}}</h3>
                <div class="img mt-4 mr-5">
                    <embed class="card-img-top category-banner img-fluid" src="{{ asset('assets/imagenes/partituras/' . $fotopartitura->nombrearchivo)}}" alt="{{ $fotopartitura->nombrearchivo }}"/>
                    <div class="overlay">
                        <div class="text">Descarga la partitura para ver más...</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
            	<label for="autor">Autor</label>
            		<li>{{$partitura->autor}}.</li>

            	<label for="epoca">Época</label>
            		<li>{{$partitura->epoca}}.</li>

            	<label for="descripcion">Descripción</label>
            		<p>{{ Str::limit($partitura->descripcion, 200, $end='...') }}</p>

                @if($datosUsuario->descargasDisponibles >= 1)
                    <a type="button" class="btn btn-info" href="{{ url('descargarPartitura/' . $partitura->id) }}">Descargar</a>
                @endif
            </div>
		</div>
        <div class="mt-3">
            <label for="comments">Comentarios</label>
            @comments([
                'model' => $partitura,
                'approved' => true
            ])
        </div>
        
	</div>
</body>
@endsection