<div id="crearPartitura" class="modal.fade modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nueva partitura</h4>
      </div>
        <div class="modal-body">
            <form action="{{ action('PartituraController@postCrearPartitura') }}" enctype="multipart/form-data" method="post">
                {{ csrf_field() }} 

                <input name="user_id" type="hidden" value="{{ $datosUsuario->id}}">
                <div class="form-group">
                    <label for="nombre">Nombre*</label>
                    <input type="text" name="nombre" class="form-control" data-placement="top" data-toggle="popover" data-trigger="focus" data-content="Escribe un nombre reconocible de la obra">
                </div>
                <div class="form-group">
                    <label for="epoca">Época*</label>
                    <select name="epoca" class="form-control">
                        <option name="edadMedia" value="Edad Media">Edad Media (S.IX-XV)</option>
                        <option name="renacimiento" value="Renacimiento">Renacimiento (1450-1600)</option>
                        <option name="barroco" value="Barroco">Barroco (1600-1750)</option>
                        <option name="clasicismo" value="Clasicismo">Clasicismo (1750-1827)</option>
                        <option name="romanticismo" value="Romanticismo">Romanticismo (1800-1880)</option>
                        <option name="impresionismo" value="Impresionismo">Impresionismo (1900-1940)</option>
                        <option name="contemporanea" value="Contemporanea">Contemporánea (1900-1940)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="autor">Autor/Compositor*</label>
                    <input type="text" name="autor" class="form-control" data-toggle="popover" data-trigger="focus" data-content="Escribe el nombre del autor o compositor real de la obra">
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción*</label>
                    <textarea name="descripcion" class="form-control" data-toggle="popover" data-trigger="focus" data-content="Escribe una pequeña descripción sobre la partitura si es una creación propia y si no lo es un pequeño contexto de la época"></textarea>
                </div>
                <div class="form-group">
                    <label for="photo"> Fotos partituras (se pueden seleccionar varias):</label><br>
                    <input multiple="multiple" name="fotos[]" type="file" class="btn btn-light"> 
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-info" value="Añadir partitura">
                </div>
            </form>
        </div>
    </div>
</div>
</div>