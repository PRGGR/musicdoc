<!DOCTYPE html>
@extends('layouts.w')
@section('titulo')
    WELCOME MUSICDOC
@endsection
@section('contenido')
<html>
<body>
    <div class="container mt-4">
      <div class="row">
        <div class="col-sm">
        </div>
        <div class="col-sm mt-5">
            <img src="assets/logo/logo_welcome.svg" width="500" alt="musicdoc">
            <h1 class="mt-3 mb-5">"La solución a tus problemas de análisis musical"</h1>
            <div class="center">
                <a href="{{ url('/home') }}" class="btn button">Inicio</a>
                <a href="{{ url('login') }}" class="btn button">Acceder</a>
                <a href="{{ url('register') }}" class="btn button">Registro</a>
            </div>
        </div>
        <div class="col-sm">
        </div>
      </div>
    </div>

</body>
</html>
    
@endsection
       
     {{-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}

