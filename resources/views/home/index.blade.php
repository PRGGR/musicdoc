@extends('layouts.master')
	<link rel="stylesheet" href="{{ url('/assets/css/home/index_style.css') }}" >
@section('titulo')
	MUSICDOC
@endsection
@section('contenido')

	<body class="indexbody">
		<div class="row">
		    <div class="col mr-5">
		     	<h1 class="mt-5">Análisis musicales<br> al alcance de <strong>todos</strong></h1>

				<p class="mt-5">A todos los estudiantes profesionales de música, en algún momento, o muchos de nuestra carrera, nos vemos en busca de un análisis musical nunca antes analizado. </p>

				<p>También puede que ese análisis sea conocido, pero no sepas muy bien como analizarlo.</p>
				<p>Gracias a esta página, todo estudiante de música, profesor o compositor, podrá buscar y encontrar aquello que busca, simplemente añadiendo uno propio ya resuelto.</p>

				<p>Con esto buscamos que se vaya ampliando nuestro repertorio a la vez que ampliando nuestros usuarios satisfechos. </p>

		    </div>

		    <div class="col ml-5 mb-5 mt-5">
			    <div class="flip-card">
				  <div class="flip-card-inner">
				    <div class="flip-card-front">
				      <img src="assets/css/home/exhibition.jpg" class="img-fluid float-right shadow p-3 mb-5 bg-white rounded" alt="exhibition" style="width:600px;">
				    </div>
				    <div class="flip-card-back">
				      	<div class="container mb-5 mt-5"> 	        
				          <div class="text-center">
				          	<img src="assets/logo/logo_blanco.svg" class="mt-5" width="300" alt="musicdoc">
							@if(Auth::check() )
				            	<a href="{{ url('partituras') }}" class="btn b mt-5">Empieza ahora</a>
							@else
				            	<a href="{{ url('register') }}" class="btn b mt-5">Únete a nosotros</a>
							@endif 
				          </div>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>

		    {{-- <div class="col ml-5 mb-5 mt-5">
				<img src="assets/css/exhibition.jpg " class="mt-4 img-fluid float-right shadow p-3 mb-5 bg-white rounded"  width="450" alt="exhibition">
				<div class="text-center">
					<a href="{{ url('register') }}" class="btn b">Únete a nosotros</a>
				</div>
		    </div> --}}
		</div>
		

	</body>

	

	{{-- <div class="row">
		@foreach($arrayAnalisis as $analisis)

			<div class="col-xs-12 col-sm-6 col-md-3" style="text-align: center;">
				<a href="{{ url('/partituras/ver/' . $analisis->id ) }}">
					<img src="{{asset('assets/imagenes/')}}/{{$analisis->imagen}}" style="height:200px" class="rounded mx-auto d-block img-thumbnail"/>
				</a>
				<h3 style="min-height:45px;margin:5px 0 10px 0; text-align: center;">
					{{$analisis->nombre}}
				</h3>
				<!-- <h5 style="font-style: italic">{{$analisis->autor}}</h5>
				<a href="{{ url('/partituras/ver/' . $analisis->id ) }}" style="font-size: 12px;">(Leer +)</a>	 -->								
			</div>
		@endforeach
	</div>
	<select name="seleccion">
		@foreach($arrayAnalisis as $analisis)
				<option value="{{$analisis->nombre}}">{{$analisis->nombre}}</option>
		@endforeach
	</select> --}}

@endsection
