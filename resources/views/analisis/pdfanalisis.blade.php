<!DOCTYPE html>
<html>
<head>
    <title>Análisis - {{$analisis->nombre}}</title>
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.css') }}" > 
    <link rel="stylesheet" href="{{ url('/assets/css/pdf/pdf_style.css') }}" >
</head>

<body>
    <header>
        <img src="{{ asset('assets/logo/logo.svg') }}" height="50">
    </header>
    <div class="container">
        <h1 class="mb-4">{{$analisis->nombre}}</h1>

        <label for="autor">Autor: </label>
        <p>{{$analisis->autor}}</p>
        <label for="autor">Época: </label>
        <p>{{$analisis->epoca}}</p>
        <label for="desarrollo">Desarrollo</label>
        <p>- {{$analisis->desarrollo}}</p>

        @foreach($fotosanalisis as $foto)
        <img src="{{ asset('assets/imagenes/analisis/' . $foto->nombrearchivo)}}" alt="{{ $foto->nombrearchivo }}" width="600" alt="">
        @endforeach
        
    </div>
    

</body>
</html>