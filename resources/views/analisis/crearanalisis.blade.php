<div id="crearAnalisis" class="modal.fade modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Nuevo análisis</h4>
        </div>
        <div class="modal-body">
            <div class="container">

             <form action="{{ action('AnalisisController@postCrearAnalisis') }}" enctype="multipart/form-data" method="post" class="">
                {{ csrf_field() }} 

                <input name="user_id" type="hidden" value="{{ $datosUsuario->id}}">
                <div class="form-group">
                    <label for="nombre">Nombre*</label>
                    <input type="text" name="nombre" class="form-control">
                </div>
                <div class="form-group">
                    <label for="epoca">Época*</label>
                    <select name="epoca" class="form-control">
                        <option name="edadMedia" value="Edad Media" >Edad Media (S.IX-XV)</option>
                        <option name="renacimiento" value="Renacimiento">Renacimiento (1450-1600)</option>
                        <option name="barroco" value="Barroco">Barroco (1600-1750)</option>
                        <option name="clasicismo" value="Clasicismo">Clasicismo (1750-1827)</option>
                        <option name="romanticismo" value="Romanticismo">Romanticismo (1800-1880)</option>
                        <option name="impresionismo" value="Impresionismo">Impresionismo (1900-1940)</option>
                        <option name="contemporanea" value="Contemporanea">Contemporánea (1900-1940)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="autor">Autor*</label>
                    <input type="text" name="autor" class="form-control">
                </div>
                <div class="form-group">
                    <label for="macroforma">Macroforma</label>
                    <input type="text" name="macroforma" class="form-control">
                </div>
                <div class="form-group">
                    <label for="microforma">Microforma</label>
                    <input type="text" name="microforma" class="form-control">
                </div>
                <div class="form-group">
                    <label for="armonia">Armonía</label>
                    <input type="text" name="armonia" class="form-control">
                </div>
                <div class="form-group">
                    <label for="completo">¿El análisis está completo?</label>
                    <label for="si">Si</label>
                    <input type="radio" id="si" name="completo" value="1">
                    <label for="no">No</label>
                    <input type="radio" id="no" name="completo" value="0">
                </div>
                <div class="form-group">
                    <label for="desarrollo">Desarrollo*</label>
                    <textarea name="desarrollo" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="foto"> Fotos análisis (se pueden seleccionar varias):</label><br>
                    <input multiple="multiple" name="fotos[]" type="file" class="btn btn-light"> 
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-info" value="Añadir análisis">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>