@extends('layouts.master')
	<link rel="stylesheet" href="{{ url('/assets/css/mostrar/mostrar_style.css') }}" >
@section('titulo')
	Mostrar
@endsection
@section('contenido')

<body class="mb-5">
    <div class="container mt-4 mb-5">
		<div class="row mt-4">		
			<div class="col-md-4">
				<h3>{{$analisis->nombre}}</h3>
                <div class="img mt-4 mr-5">
                    <embed class="card-img-top category-banner img-fluid" src="{{ asset('assets/imagenes/analisis/' . $fotoanalisis->nombrearchivo)}}" alt="{{ $fotoanalisis->nombrearchivo }}"/>
                    <div class="overlay">
                        <div class="text">Descarga el análisis para ver más...</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-md-6">
                    	<label for="autor">Autor</label>
                    		<li>{{$analisis->autor}}.</li>
                    	<label for="epoca">Época</label>
                    		<li>{{$analisis->epoca}}.</li>
                        <label for="armonia">Armonía</label>
                            <li>{{$analisis->armonia}}</li>
                    </div>
                    <div class="col-md-6">
                        <label for="macroforma">Macroforma</label>
                            <li>{{$analisis->macroforma}}.</li>
                        <label for="microforma">Microforma</label>
                            <li>{{$analisis->microforma}}.</li>
                        <label for="completo">¿Está completo?</label>
                        @if($analisis->completo == 1)
                            <li><i class="fas fa-check"></i></li>
                        @else
                            <li><i class="fas fa-times"></i></li>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <label for="desarrollo">Desarrollo</label>
                        <p>{{ Str::limit($analisis->desarrollo, 200, $end='...') }}</p>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        @if($datosUsuario->descargasDisponibles >= 1)
                            <a type="button" class="btn btn-info" href="{{ url('descargarAnalisis/' . $analisis->id) }}">Descargar</a>
                        @endif
                    </div>
                    <div class="col-gd-3">
                        <a href="{{ url('/analisis/like/' . $analisis['sluganalisis'] . '/' . $analisis['id'] ) }}">
                            <span class="btn btn-light" id="like">
                                @if($likeUsuario == 0)
                                    <span class="iconify" data-icon="clarity:heart-line" data-inline="false" style="color: red;" data-width="20px" data-height="20px"></span> Me gusta ({{$analisis->likesAnalisis->count()}})
                                @else
                                    <span class="iconify" data-icon="clarity:heart-solid" data-inline="false" style="color: red;" data-width="20px" data-height="20px"></span> Te gusta el análisis({{$analisis->likesAnalisis->count()}})
                                @endif
                            </span>
                        </a>
                    </div>
                    <div class="col-gd-3">
                        <a href="{{ url('/analisis/dislike/' . $analisis['sluganalisis'] . '/' . $analisis['id'] ) }}">
                            <span class="btn btn-light" id="dislike">
                                @if($dislikeUsuario == 0)
                                    <span class="iconify" data-icon="clarity:heart-broken-line" data-inline="false" style="color: black;" data-width="20px" data-height="20px"></span> No me gusta ({{$analisis->dislikesAnalisis->count()}})
                                @else
                                    <span class="iconify" data-icon="clarity:heart-broken-solid" data-inline="false" style="color: black;" data-width="20px" data-height="20px"></span> No te gusta el análisis ({{$analisis->dislikesAnalisis->count()}})
                                @endif
                            </span>
                        </a>
                    </div>
                </div>
            </div>
		</div>

        <br>

        <div class="mt-3">
            <label for="comments">Comentarios</label>
            @comments([
                'model' => $analisis,
                'approved' => true
            ])
        </div>
        
	</div>
</body>
@endsection