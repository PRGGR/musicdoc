<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

	//INICIO
	Route::get('/', 'InicioController@getWelcome');
	Route::get('home', 'InicioController@getHome');


Route::group(['middleware' => 'auth'], function(){

	/*********************************** PERFIL *****************************/

		// PERFIL
		Route::get('profile', 'ProfileController@getProfile');

		// EDITAR PERFIL
		Route::get('profile/edit/{id}', 'ProfileController@getEditar');
		Route::post('profile/edit/{id}', 'ProfileController@postEditar');

	/********************************** PARTITURAS **************************/

		//CREAR PARTITURAS
		Route::get('partituras/crear','PartituraController@getCrearPartitura');
		Route::post('partituras/crear','PartituraController@postCrearPartitura');

		// VER PARTITURAS
		Route::get('partituras','PartituraController@getListado');
		Route::get('partituras/ver/{slugpartitura}/{id}', 
			'PartituraController@getVer')->where('id', '[0-9]+');

		// VER LISTADO PARTITURAS ORDENADAS
		Route::post('partiturasordenadas','PartituraController@getFiltros');

		// DESCARGAR PARTITURA
		Route::get('descargarPartitura/{id}', 
		'PartituraController@descargarPartitura')->where('id', '[0-9]+');

		// BUSCAR PARTITURAS
		Route::post('busquedaAjax', 'PartituraController@buscarPartituraNombre');
		Route::post('partituras/filtradas', 'PartituraController@partiturasBuscadas');


	/********************************** ANÁLISIS **************************/

		//CREAR ANÁLISIS
		Route::get('analisis/crear','AnalisisController@getCrearAnalisis');
		Route::post('analisis/crear','AnalisisController@postCrearAnalisis');

		// VER ANÁLISIS
		Route::get('analisis','AnalisisController@getListadoAnalisis');
		Route::get('analisis/ver/{sluganalisis}/{id}', 
			'AnalisisController@getVer')->where('id', '[0-9]+');
	
		// VER LISTADO ANÁLISIS ORDENADOS
		Route::post('analisisordenados','AnalisisController@getFiltros');

		// DESCARGAR ANÁLISIS
		Route::get('descargarAnalisis/{id}', 
		'AnalisisController@descargarAnalisis')->where('id', '[0-9]+');

		//LIKE ANÁLISIS
		Route::get('/analisis/like/{sluganalisis}/{id}','AnalisisController@Like');
		Route::get('/analisis/dislike/{sluganalisis}/{id}','AnalisisController@Dislike');

		// BUSCAR ANÁLISIS
		Route::post('busquedaAjaxAnalisis', 'AnalisisController@buscarAnalisisNombre');
		Route::post('analisis/filtrados', 'AnalisisController@analisisBuscados');
	
});





	