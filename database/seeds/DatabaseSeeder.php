<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Partitura;
use App\PartiturasFoto;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    private $arrayPartituras = array(
		array(
			'user_id' => 1,
			'nombre' => 'Concierto para flauta',
			'epoca' => 'Clasicismo',
			'autor' => 'J. Stamitz',
			'descripcion' => 'A pioneer in the development of the symphony, Johann Stamitz also composed several concertos, including fourteen for flute. The four flute concertos presented here show what delightful results came from the combination of Stamitz’s lively musical imagination with the talents of the highly admired Mannheim orchestra’s players for whom they were written. Virtuosity, lyricism and a dash of Bohemian spirit characterise these charming works, in which the emergence of the Classical style from its Baroque antecedents can be heard.'
		),
		array(
			'user_id' => 1,
			'nombre' => 'Partita para flauta sola',
			'epoca' => 'Barroco',
			'autor' => 'Johann Sebastian Bach',
			'descripcion' => 'La Partita para flauta sola en la menor, BWV 1013 es una partita escrita para flauta sola en cuatro movimientos por Johann Sebastian Bach probablemente alrededor de 1718.1

				En función de su técnica de interpretación avanzada, que es más exigente que la parte de flauta para el Concierto de Brandenburgo n.º 5 por ejemplo, sin duda debe haber sido escrita después de 1723.2​El título, sin embargo, es obra de los editores del siglo XX. El título del único manuscrito del siglo XVIII que se conserva es "Solo p[our une] flûte traversière par J. S. Bach".'
						),
		array(
			'user_id' => 1,
			'nombre' => 'Concierto para flauta n.º 1',
			'epoca' => 'Clasicismo',
			'autor' => 'Wolfgang Amadeus Mozart',
			'descripcion' => 'La obra fue encargada por el flautista holandés Ferdinand De Jean en 1777, parece ser que Mozart le proporcionó cuatro cuartetos para flauta y tres conciertos para flauta, aunque solo llegó a completar dos de los tres conciertos: el KV 313 sería el primer'
		),
        array(
            'user_id' => 1,
            'nombre' => 'Danza de la Cabra',
            'epoca' => 'Contemporanea',
            'autor' => 'Arthur Honegger',
            'descripcion' => 'Danse de la chèvre (traducido del francés, Danza de la cabra) es una pieza para flauta solo por Arthur Honegger, compuesta en 1921 como música incidental para la bailarina Lysana de la obra teatral La mauvaise pensée de Sacha Derek'
        ),
	);


    private $arrayPartiturasFoto = array(
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-01.jpg'),
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-02.jpg'),
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-03.jpg'),
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-04.jpg'),
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-05.jpg'),
    	array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-06.jpg'),
		array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-07.jpg'),
		array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-08.jpg'),
		array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-09.jpg'),
		array('partitura_id' => 1,
    		  'nombrearchivo' => 'Concierto para flauta-J.Stamitz-10.jpg'),
		array('partitura_id' => 2,
    		  'nombrearchivo' => 'Partita_1.jpg'),
		array('partitura_id' => 2,
    		  'nombrearchivo' => 'Partita_2.jpg'),
		array('partitura_id' => 3,
    		  'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_1.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_2.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_3.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_4.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_5.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_6.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_7.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_8.jpg'),
        array('partitura_id' => 3,
              'nombrearchivo' => 'Mozart_Flute_Concerto_K313_sol mayor_9.jpg'),
        array('partitura_id' => 4,
              'nombrearchivo' => 'Danza de la cabra_1.jpg'),
        array('partitura_id' => 4,
              'nombrearchivo' => 'Danza de la cabra_2.jpg'),
    );





    public function run()
    {
        self::seedUsers();
 		$this->command->info('Tabla usuarios inicializada con datos');

 		self::seedPartituras();
 		$this->command->info('Tabla partitura inicializada con datos');

 		self::seedPartiturasFoto();
 		$this->command->info('Tabla partiturasfoto inicializada con datos');
    }

    private function seedUsers(){

    	DB::table('users')->delete();

    	$u = new User();
    	$u->name = "Paula";
    	$u->surname = "Rodríguez García";
    	$u->email = "paularodriguez451@gmail.com";
    	$u->password = bcrypt("usuario@1");
        $u->photo = "3d06149675c22fb5403ae7b083cc779c";
		$u->save();
		
    }

    private function seedPartituras(){

    	DB::table('partituras')->delete();

    	foreach ($this->arrayPartituras as $partitura){
			$p = new Partitura();
			$p->user_id = User::all()->first()->id;
			$p->nombre = $partitura['nombre'];
			$p->epoca = $partitura['epoca'];
			$p->autor = $partitura['autor'];
			$p->descripcion = $partitura['descripcion'];
            $p->slugpartitura = Str::slug($partitura['nombre']);
			$p->save();
		}
    }

    private function seedPartiturasFoto(){

    	DB::table('partiturasfoto')->delete();

    	foreach ($this->arrayPartiturasFoto as $foto){
			$f = new PartiturasFoto();
			$f->partitura_id = Partitura::all()->first()->id;
			$f->nombrearchivo = $foto['nombrearchivo'];
			$f->save();
		}
    }
}
