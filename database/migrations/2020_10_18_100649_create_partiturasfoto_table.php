<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartiturasfotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partiturasfoto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('partitura_id');
            $table->foreign('partitura_id')->references('id')->on('partituras')->onDelete('cascade');;
            $table->string('nombrearchivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partiturasfoto');
    }
}
