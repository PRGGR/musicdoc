<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalisisfotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisisfoto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('analisis_id');
            $table->foreign('analisis_id')->references('id')->on('analisis')->onDelete('cascade');;
            $table->string('nombrearchivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisisfoto');
    }
}
