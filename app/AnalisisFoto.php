<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Analisis;

class AnalisisFoto extends Model
{
    protected $table = 'analisisfoto';
    protected $fillable = ['analisis_id', 'nombrearchivo'];

    public function product(){
        return $this->belongsTo('App\Analisis');
    }
}
