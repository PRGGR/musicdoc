<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;
use Laravelista\Comments\Comment;
use App\User;

class Analisis extends Model
{
	use Commentable;

    protected $table = 'analisis';

    public function analisis(){
        return $this->belongsTo('App\User');
    }

    public function comentariosAnalisis(){
    	return $this->hasMany('Laravelista\Comments\Comment','commentable_id','id')->where('commentable_type', 'App\Analisis');;
    }

    public function likesAnalisis(){
        return $this->hasMany('App\Like');
    }

    public function dislikesAnalisis(){
        return $this->hasMany('App\Dislike');
    }
}
