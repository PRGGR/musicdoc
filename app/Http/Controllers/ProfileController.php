<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Partitura;
use App\Analisis;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function getProfile(){
        $usuario = Auth::user();
        $partituras = Partitura::all()->where('user_id', '=', $usuario->id);
        $analisis = Analisis::all()->where('user_id', '=', $usuario->id);

        return view('perfil.profile', array('datosUsuario' => $usuario, 
                                            'datosPartituras' => $partituras,
                                            'datosAnalisis' => $analisis));
    }


    public function getEditar($id){
        $usuario = Auth::user();
        return view('perfil.edit-profile', array('datosUsuario' => $usuario));
    }

    public function postEditar(Request $request,$id){
        $usuario = User::findOrFail($id);
        $usuario->name            	= $request->name;
        $usuario->surname           = $request->surname;
        $usuario->email             = $request->email;
        $usuario->password   		= Hash::make($request->password);
        
        try{
            $usuario->save();
            return redirect('profile')->with('success', "Se ha editado correctamente al usuario");
        } catch(Exception $ex){
            return redirect('profile')->withErrors(["Fallo al editar el usuario"]);
        }
    }

}