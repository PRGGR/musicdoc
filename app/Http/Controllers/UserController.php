<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

//------------------------- DESCARGAS DISPONIBLES --------------------------------

    public function sumarDescargasDisponibles($idUsuario){
        User::findOrFail($idUsuario)->increment('descargasDisponibles', 1);
    }

    public function restarDescargasDisponibles($idUsuario){
        $usuario = User::findOrFail($idUsuario);

        if($usuario->descargasDisponibles > 0){
            $usuario->decrement('descargasDisponibles', 1);
            return true;
        } else{
            return false;
        }
    }

}
