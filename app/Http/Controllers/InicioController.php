<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class InicioController extends Controller
{
	public function getWelcome(){
		return view('home.welcome'); 
    }

    public function getHome(){
    	$usuario = Auth::user();
        // print_r($usuario);
    	return view('home.index', array('datosUsuario' => $usuario));
    }

}
