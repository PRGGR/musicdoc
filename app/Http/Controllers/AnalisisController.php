<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as DESCARGARPDF;
use Illuminate\Support\Facades\Storage;
use Org_Heigl\Ghostscript\Ghostscript;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\PdfToImage\Pdf;
use App\AnalisisFoto;
use App\Analisis;
use App\Dislike;
use App\Like;
use Auth;

class AnalisisController extends Controller
{

//--------------------------------------------------------------------------------
//----------------------------- CREAR ANÁLISIS ----------------------------------

    public function getCrearAnalisis(){
        $usuario = Auth::user();
        return view('analisis.crearanalisis', 
            array('datosUsuario' => $usuario));
    }

    public function postCrearAnalisis(Request $request){
        $request->validate([
            'nombre' => 'required|min:6',
            'epoca' => 'required',
            'autor' => 'required|min:10',
            'desarrollo' => 'required|min:255',
            'fotos' => 'required'
        ]);

        $analisis = new Analisis();
        $analisis->nombre = $request->nombre;
        $analisis->epoca = $request->epoca;
        $analisis->autor = $request->autor;
        $analisis->macroforma = $request->macroforma;
        $analisis->microforma = $request->microforma;
        $analisis->armonia = $request->armonia;
        $analisis->completo = $request->completo;
        $analisis->desarrollo = $request->desarrollo;
        $analisis->user_id = $request->user_id;
        $analisis->sluganalisis = Str::slug($request->nombre);

        try {
            if($request->hasFile('fotos')) {
                $analisis->save();
                app('App\Http\Controllers\UserController')->sumarDescargasDisponibles($analisis->user_id);

                $archivo = $request->fotos;
                $nombreImagen = $archivo[0]->getClientOriginalName();
                $extension = substr($nombreImagen, -4);
                $nombresinextension = substr($nombreImagen,0, (strlen($nombreImagen)) 
                    - (strlen (strrchr($nombreImagen,'.'))));

                if($extension == '.pdf'){
                    foreach ($request->fotos as $pdf) {
                        $this->pdftoJPG($pdf,$nombresinextension);  
                        for ($i=1; $i <= $this->numPaginasPDF($pdf) ; $i++) { 
                            ${"fotoanalisis" . $i} = new AnalisisFoto();
                            ${"fotoanalisis" . $i}->analisis_id = $analisis->id;
                            ${"fotoanalisis" . $i}->nombrearchivo = $nombresinextension . '_a_'. $i .'.jpg'; 
                            try{
                                ${"fotoanalisis" . $i}->save();
                            } catch(Exception $ex){
                                return redirect('analisis')->withErrors(['Fallo al subir imágenes']);
                            }  
                        }
                        return redirect('analisis');
                    }  
                } else{
                    foreach ($request->fotos as $jpg) {
                        $nombre = $jpg->getClientOriginalName();
                        $jpg->move(public_path('assets/imagenes/analisis'), $nombre);
                        $fotoanalisis = new AnalisisFoto();
                        $fotoanalisis->analisis_id = $analisis->id;
                        $fotoanalisis->nombrearchivo = $nombre;
                        try{
                            $fotoanalisis->save();
                        } catch(Exception $ex){
                            return redirect('analisis')->withErrors(['Fallo al subir imágenes']);
                        } 
                    }
                    return redirect('analisis')->with('success', "Se ha creado el análisis con éxito.");
                }
            }
        } catch (Exception $ex){
            return redirect('analisis')->withErrors(['Fallo al crear el análisis']);
        }
    }

    public function pdftoJPG($file,$nombresinextension){

        Ghostscript::setGsPath("C:\Program Files\gs\gs9.54.0\bin\gswin64c.exe");
        $gs = new Ghostscript ();

        $gs->setDevice('jpeg')
            ->setInputFile($file)
            ->setOutputFile($nombresinextension . '_a_%d.jpg')
            ->setResolution(1000)
            ->setTextAntiAliasing(Ghostscript::ANTIALIASING_HIGH)->getDevice()->setQuality(100);

        $gs->render();
    }

    public function numPaginasPDF($pdf){
        $fp = @fopen(preg_replace("/\[(.*?)\]/i", "", $pdf), "r");
        $max = 0;
        while(!feof($fp)) {
                $line = fgets($fp,255);
                if (preg_match('/\/Count [0-9]+/', $line, $matches)){
                        preg_match('/[0-9]+/', $matches[0], $matches2);
                        if ($max < $matches2[0]) $max = $matches2[0];
                }
        }
        fclose($fp);
        return (int) $max;
    }

//--------------------------------------------------------------------------------
//------------------------------ VER ANÁLISIS ------------------------------------

    public function getVer($sluganalisis,$id){
        $usuario = Auth::user();

        $analisis = Analisis::where('sluganalisis',$sluganalisis)->first();
        $fotoanalisis = AnalisisFoto::where('analisis_id', $id)->first();

        $analisis_id = Analisis::find($id);
        $likeUsuario = Like::where(['user_id'=> $usuario->id, 'analisis_id' => $id])->count();

        $disLikeUsuario = Dislike::where(['user_id'=> $usuario->id, 'analisis_id' => $id])->count();

        return view('analisis.mostraranalisis', array('datosUsuario' => $usuario,
              'analisis' => $analisis,
              'fotoanalisis' => $fotoanalisis,
              'likeUsuario' => $likeUsuario,
              'dislikeUsuario' => $disLikeUsuario
          ));
    }

    public function getListadoAnalisis(){
        $usuario = Auth::user();
        $listadoAnalisis = Analisis::all();
        $fotos = [];

        foreach ($listadoAnalisis as $analisis) {
            $foto = AnalisisFoto::where('analisis_id', '=', $analisis->id)->pluck('nombrearchivo')->first();
            $fotos[] = ['id'=> $analisis->id, 
            'nombre' => $analisis->nombre,
            'autor' => $analisis->autor, 
            'nombrearchivo' => $foto,
            'sluganalisis' => $analisis->sluganalisis];
        }
        return view('analisis.listadoanalisis', array('datosUsuario' => $usuario,
            'listadoAnalisisFoto' => $fotos)); 
    }

//--------------------------------------------------------------------------------
//-------------------------- VER ANÁLISIS ORDENADOS ------------------------------

    public function getFiltros(Request $request){
        $orden = $request->orden;
        $usuario = Auth::user();
        $listadoAnalisis = Analisis::all();
        $fotos = [];

        if($orden == 'nombre'){
            $listadoAnalisis = Analisis::reorder('nombre', 'asc')->get();
        } elseif ($orden == 'fecha') {
            $listadoAnalisis = Analisis::reorder('created_at', 'desc')->get();
        } elseif ($orden == 'likes') {
            $listadoAnalisis = Analisis::with('likesAnalisis')->get()->sortByDesc(
                function($a){
                    return $a->likesAnalisis->count();
                });
        } elseif ($orden == 'comments') {
            $listadoAnalisis = Analisis::with('comentariosAnalisis')->get()->sortByDesc(
                function($a){
                    return $a->comentariosAnalisis->count();
                });
        } else{
            return redirect('analisis')->withErrors(['No se ha seleccionado ningún filtro']);
        }


        foreach ($listadoAnalisis as $analisis) {
            $foto = AnalisisFoto::where('analisis_id', '=', $analisis->id)->pluck('nombrearchivo')->first();
            $fotos[] = ['id'=> $analisis->id, 
            'nombre' => $analisis->nombre,
            'autor' => $analisis->autor, 
            'nombrearchivo' => $foto,
            'sluganalisis' => $analisis->sluganalisis];
        }

        return view('analisis.listadoAnalisis', array('datosUsuario' => $usuario,
            'listadoAnalisisFoto' => $fotos))->with('msg', "Se han ordenado los análisis por: ". $orden);
    }

//--------------------------------------------------------------------------------
//---------------------------- DESCARGAR ANÁLISIS --------------------------------

    public function descargarAnalisis($idAnalisis){

        $usuario = Auth::user();

        if(app('App\Http\Controllers\UserController')->restarDescargasDisponibles($usuario->id)){

            $analisis = Analisis::findOrFail($idAnalisis);
            $fotosanalisis = AnalisisFoto::where('analisis_id', $idAnalisis)->get();

            $pdf = DESCARGARPDF::loadView('analisis.pdfanalisis', 
                                ['analisis' => $analisis], 
                                ['fotosanalisis' => $fotosanalisis]);

            return $pdf->download($analisis->nombre . '.pdf');
        } else{
            return redirect('analisis')->withErrors(['Fallo al descargar el análisis']);
        }
    }

//--------------------------------------------------------------------------------
//------------------------------- BUSCAR ANÁLISIS --------------------------------

    public function buscarAnalisisNombre(Request $request){
        $search = $request->busqueda;

        if($search != ''){
            $analisis = Analisis::orderby('nombre','asc')->select('id','nombre')->where('nombre', 'like', '%' .$search . '%')->limit(5)->get();
            foreach($analisis as $a){
                $response[] = array("label"=>$a->nombre);
            }
        }
        return response()->json($response);
    }

    public function analisisBuscados(Request $request){
        $busqueda = $request->busqueda;
        $analisis = Analisis::where('nombre',$busqueda)->first();
        if($analisis !=''){
            return $this->getVer($analisis->sluganalisis,$analisis->id);
        } else{
            return redirect('partituras')->withErrors(['No se ha seleccionado ningún análisis para buscar']);
        }
    }

//--------------------------------------------------------------------------------
//--------------------------- LIKE/DISLIKE ANÁLISIS ------------------------------

    public function like($sluganalisis,$id){

        $usuarioID = Auth::user()->id;
        $likeUsuario = Like::where(['user_id'=> $usuarioID, 'analisis_id' => $id])->first();
        $dislikeUsuario = Dislike::where(['user_id'=> $usuarioID, 'analisis_id' => $id])->count();

        if(empty($likeUsuario->user_id) && $likeUsuario != 1){
            $like = new Like();
            $like->user_id = $usuarioID;
            $like->analisis_id = $id;
            $like->save();

            if($dislikeUsuario == 1){
                Dislike::where(['user_id'=> $usuarioID,'analisis_id'=>$id])->get()->first()->delete();
                return $this->getVer($sluganalisis,$id);
            } 
            return $this->getVer($sluganalisis,$id);

        } else{
            return $this->getVer($sluganalisis,$id)->withErrors(['Ya le has dado a "me gusta"']);
        }
    }

    public function dislike($sluganalisis,$id){

        $usuarioID = Auth::user()->id;
        $dislikeUsuario = Dislike::where(['user_id'=> $usuarioID, 'analisis_id' => $id])->count();
        $likeUsuario = Like::where(['user_id'=> $usuarioID, 'analisis_id' => $id])->count();

        if(empty($dislikeUsuario->user_id) && $dislikeUsuario != 1){
            $dislike = new Dislike();
            $dislike->user_id = $usuarioID;
            $dislike->analisis_id = $id;
            $dislike->save();

            if($likeUsuario == 1){
                Like::where(['user_id'=> $usuarioID,'analisis_id'=>$id])->get()->first()->delete();
                return $this->getVer($sluganalisis,$id);
            } 
            return $this->getVer($sluganalisis,$id);

        } else{
            return $this->getVer($sluganalisis,$id)->withErrors(['Ya le has dado a "no me gusta"']);
        }
    }


}
