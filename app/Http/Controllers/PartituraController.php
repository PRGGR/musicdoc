<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as DESCARGARPDF;
use Org_Heigl\Ghostscript\Ghostscript;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\PdfToImage\Pdf;
use App\PartiturasFoto;
use App\Partitura;
use App\User;
use Auth;

class PartituraController extends Controller
{

//--------------------------------------------------------------------------------
//----------------------------- CREAR PARTITURA ----------------------------------

    public function getCrearPartitura(){
        $usuario = Auth::user();
        return view('partituras.crearpartitura', 
            array('datosUsuario' => $usuario));
    }

    public function postCrearPartitura(Request $request){

        $request->validate([
            'nombre' => 'required|min:6',
            'epoca' => 'required',
            'autor' => 'required|min:10',
            'descripcion' => 'required|min:255',
            'fotos' => 'required'
        ]);

        $partitura = new Partitura();
        $partitura->nombre = $request->nombre;
        $partitura->epoca = $request->epoca;
        $partitura->autor = $request->autor;
        $partitura->descripcion = $request->descripcion;
        $partitura->user_id = $request->user_id;
        $partitura->slugpartitura = Str::slug($request->nombre);

        try {
            if($request->hasFile('fotos')) {
                $partitura->save();
                app('App\Http\Controllers\UserController')->sumarDescargasDisponibles($partitura->user_id);

                $archivo = $request->fotos;
                $nombreImagen = $archivo[0]->getClientOriginalName();
                $extension = substr($nombreImagen, -4);
                $nombresinextension = substr($nombreImagen,0, (strlen($nombreImagen)) 
                    - (strlen (strrchr($nombreImagen,'.'))));

                if($extension == '.pdf'){
                    foreach ($request->fotos as $pdf) {
                        $this->pdftoJPG($pdf,$nombresinextension);  
                        for ($i=1; $i <= $this->numPaginasPDF($pdf) ; $i++) { 
                            ${"fotopartitura" . $i} = new PartiturasFoto();
                            ${"fotopartitura" . $i}->partitura_id = $partitura->id;
                            ${"fotopartitura" . $i}->nombrearchivo = $nombresinextension . '_p_'. $i .'.jpg'; 
                            try{
                                ${"fotopartitura" . $i}->save();
                            } catch(Exception $ex){
                                 return redirect('partituras')->withErrors(['Fallo al subir imágenes']);
                            }  
                        }
                        return redirect('partituras');   
                    }  
                } else{
                    foreach ($request->fotos as $jpg) {
                        
                        $nombre = $jpg->getClientOriginalName();
                        $jpg->move(public_path('assets/imagenes/partituras'), $nombre);
                        $fotopartitura = new PartiturasFoto();
                        $fotopartitura->partitura_id = $partitura->id;
                        $fotopartitura->nombrearchivo = $nombre;

                        try{
                            $fotopartitura->save();
                        } catch(Exception $ex){
                            return redirect('partituras')->withErrors(['Fallo al subir imágenes']);
                        } 
                    }
                    return redirect('partituras')->with('success', "Se ha creado la partitura con éxito.");
                }
            }
        } catch (Exception $ex){
            return redirect('partituras')->withErrors(['Fallo al crear la partitura']);
        }
    }

    public function pdftoJPG($file,$nombresinextension){

        Ghostscript::setGsPath("C:\Program Files\gs\gs9.54.0\bin\gswin64c.exe");
        $gs = new Ghostscript ();

        $gs->setDevice('jpeg')
            ->setInputFile($file)
            ->setOutputFile($nombresinextension . '_p_%d.jpg')
            ->setResolution(1000)
            ->setTextAntiAliasing(Ghostscript::ANTIALIASING_HIGH)->getDevice()->setQuality(100);

        $gs->render();
    }

    public function numPaginasPDF($pdf){
        $fp = @fopen(preg_replace("/\[(.*?)\]/i", "", $pdf), "r");
        $max = 0;
        while(!feof($fp)) {
                $line = fgets($fp,255);
                if (preg_match('/\/Count [0-9]+/', $line, $matches)){
                        preg_match('/[0-9]+/', $matches[0], $matches2);
                        if ($max < $matches2[0]) $max = $matches2[0];
                }
        }
        fclose($fp);
        return (int) $max;
    }

//--------------------------------------------------------------------------------
//----------------------------- VER PARTITURA ------------------------------------
    
    public function getVer($slugpartitura,$id){
        $usuario = Auth::user();

        $partitura = Partitura::where('slugpartitura',$slugpartitura)->first();
        $fotopartitura = PartiturasFoto::where('partitura_id', $id)->first();

        return view('partituras.mostrarpartitura', array('datosUsuario' => $usuario,
          'partitura' => $partitura,
          'fotopartitura' => $fotopartitura
        ));
    }

    public function getListado(){

        $usuario = Auth::user();
        $listadoPartituras = Partitura::all();
        $fotos = [];

        foreach ($listadoPartituras as $partitura) {
            $foto = PartiturasFoto::where('partitura_id', '=', $partitura->id)->pluck('nombrearchivo')->first();
            $fotos[] = ['id'=> $partitura->id, 
            'nombre' => $partitura->nombre,
            'autor' => $partitura->autor, 
            'nombrearchivo' => $foto,
            'slugpartitura' => $partitura->slugpartitura];
        }

        return view('partituras.listado', array('datosUsuario' => $usuario,
            'listadoPartiturasFoto' => $fotos));
    }

//--------------------------------------------------------------------------------
//------------------------ VER PARTITURAS ORDENADAS ------------------------------

    public function getFiltros(Request $request){
        $orden = $request->orden;
        $usuario = Auth::user();
        $listadoPartituras = Partitura::all();

        if($orden == 'nombre'){
            $listadoPartituras = Partitura::reorder('nombre', 'asc')->get();
        } elseif ($orden == 'fecha') {
            $listadoPartituras = Partitura::reorder('created_at', 'desc')->get();
        } elseif ($orden == 'comments') {
            $listadoPartituras = Partitura::with('comentariosPartitura')->get()->sortByDesc(
                function($p){
                    return $p->comentariosPartitura->count();
                });
        } else{
            return redirect('partituras')->withErrors(['No se ha seleccionado ningún filtro']);
        }

        $fotos = [];

        foreach ($listadoPartituras as $partitura) {
            $foto = PartiturasFoto::where('partitura_id', '=', $partitura->id)->pluck('nombrearchivo')->first();
            $fotos[] = ['id'=> $partitura->id, 
            'nombre' => $partitura->nombre,
            'autor' => $partitura->autor, 
            'nombrearchivo' => $foto,
            'slugpartitura' => $partitura->slugpartitura];
        }

        return view('partituras.listado', array('datosUsuario' => $usuario,
            'listadoPartiturasFoto' => $fotos))->with('msg', "Se han ordenado las partituras por: ". $orden);
    }

//--------------------------------------------------------------------------------
//-------------------------- DESCARGAR PARTITURAS --------------------------------

    public function descargarPartitura($idPartitura){

        $usuario = Auth::user();

        if(app('App\Http\Controllers\UserController')->restarDescargasDisponibles($usuario->id)){

            $partitura = Partitura::findOrFail($idPartitura);
            $fotospartitura = PartiturasFoto::where('partitura_id', $idPartitura)->get();

            $pdf = DESCARGARPDF::loadView('partituras.pdf', 
                                ['partitura' => $partitura], 
                                ['fotospartitura' => $fotospartitura]);

            return $pdf->download($partitura->nombre . '.pdf');
        } else{
            return redirect('partituras')->withErrors(['No tienes suficientes descargas disponibles']);
        }
    }

//--------------------------------------------------------------------------------
//--------------------------- BUSCAR PARTITURAS ----------------------------------

    public function buscarPartituraNombre(Request $request){
        $search = $request->busqueda;

        if($search != ''){
            $partituras = Partitura::orderby('nombre','asc')->select('id','nombre')->where('nombre', 'like', '%' .$search . '%')->limit(5)->get();
            foreach($partituras as $p){
                $response[] = array("label"=>$p->nombre);
            }
        }
        return response()->json($response);
    }

    public function partiturasBuscadas(Request $request){
        $busqueda = $request->busqueda;
        $partitura = Partitura::where('nombre',$busqueda)->first();
        if($partitura !=''){
            return $this->getVer($partitura->slugpartitura,$partitura->id);
        } else{
            return redirect('partituras')->withErrors(['No se ha seleccionado ninguna partitura para buscar']);
        }
    }


}