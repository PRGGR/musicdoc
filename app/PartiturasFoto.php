<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Partitura;

class PartiturasFoto extends Model
{
	protected $table = 'partiturasfoto';
    protected $fillable = ['partitura_id', 'nombrearchivo'];

    public function product(){
        return $this->belongsTo('App\Partitura');
    }
}
