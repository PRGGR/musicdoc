<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;
use Laravelista\Comments\Comment;
use App\User;

class Partitura extends Model
{
    use Commentable;
	
	protected $table = 'partituras';
    protected $fillable = ['nombre'];

    public function partitura(){
        return $this->belongsTo('App\User');
    }

    public function comentariosPartitura(){
    	return $this->hasMany('Laravelista\Comments\Comment','commentable_id','id')->where('commentable_type', 'App\Partitura');
    }


}

